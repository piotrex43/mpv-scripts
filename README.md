## Overview
This repo consists of at least one script made for my personal use.

### Stream Speed Manager
This script changes the currently used speed based on how much more livestream is in the cache. I dislike missing moments so changing speed is best solution for me. 
Activate using Ctrl+s. Script automatically stops when you manually change the speed or Ctrl+s is used once again.

## License
All scripts in this repo are licensed under CC0 1.0.
