-- Quickly hacked together script for automatically changing speed of a video/stream (script made in livestreams in mind)
-- When started (ctrl+s) it will start readjusting the speed based on how far away are you from livestream "live" part
-- At maximum it can change the speed to up to 1.4 at 2 minutes of cache, it returns to regular speed on 9 seconds or less


local running = true
local last_speed_value_set_by_script = nil
local timer

function linear_speed_changer(target)
	local current_speed = tonumber(mp.get_property("speed"))
	if current_speed == target then
		return
	end
	threshold = 0.0001
	if threshold < math.abs(current_speed - last_speed_value_set_by_script) then
		mp.osd_message("Disabling the script due to manual speed change... (" .. tostring(current_speed) .. " != " .. tostring(last_speed_value_set_by_script) .. ")", 5)
		timer:stop()
		return
	end
	local smoothing = 0.025
	if math.abs(current_speed - target) < 0.025 then
		smoothing = math.abs(current_speed - target)
	end
	if current_speed > target then
		smoothing = -smoothing
	end
	mp.set_property("speed", current_speed + smoothing)
	last_speed_value_set_by_script = current_speed + smoothing
	mp.osd_message("Changing speed to: " .. tostring(current_speed + smoothing), 1)
end

function calculate_target_speed()
    local target = 1
    local timeremaining = tonumber(mp.get_property("time-remaining"))
    if timeremaining > 120 then
		target = 1.4
	elseif timeremaining > 90 then
		target = 1.3
	elseif timeremaining > 30 then
		target = 1.2
	elseif timeremaining > 9 then
		target = 1.1
	else
		target = 1.0
	end
	return target
end

function manage_speed()
    if running then
		last_speed_value_set_by_script = tonumber(mp.get_property("speed"))
        timer = mp.add_periodic_timer(2, function()
            linear_speed_changer(calculate_target_speed())
        end)
        running = false
    else
        timer:stop()
        running = true
    end
end

mp.add_key_binding("ctrl+s", "manage_speed", manage_speed)
